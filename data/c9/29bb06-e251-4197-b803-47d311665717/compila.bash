#!/bin/bash

LIBNINA_PATH=$HOME/svn/gabriel/libnina/src/
OPARI2_PATH=$HOME/install/scorep-3.1/
export PATH=$OPARI2_PATH/bin/:$PATH

opari2 main.cpp
opari2 ../utils/BoxPartition.cpp
opari2 main.cpp
opari2 ../utils/BoxPartition.cpp
opari2 YAML_Doc.cpp
opari2 YAML_Element.cpp
opari2 ../utils/param_utils.cpp
opari2 ../utils/utils.cpp
opari2 ../utils/mytimer.cpp

g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c main.mod.cpp -o main.mod.o
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c ../utils/BoxPartition.mod.cpp -o ../utils/BoxPartition.mod.o
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c YAML_Doc.mod.cpp -o YAML_Doc.mod.o 
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c YAML_Element.mod.cpp -o YAML_Element.mod.o
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c ../utils/param_utils.mod.cpp -o ../utils/param_utils.mod.o
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c ../utils/utils.mod.cpp -o ../utils/utils.mod.o
g++ -O3 -fopenmp -I. -I${OPARI2_PATH}/include -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX -DMINIFE_INFO=1 -DMINIFE_KERNELS=0 -c ../utils/mytimer.mod.cpp -o ../utils/mytimer.mod.o

$(opari2-config --nm) main.mod.o ../utils/BoxPartition.mod.o YAML_Doc.mod.o YAML_Element.mod.o ../utils/param_utils.mod.o ../utils/utils.mod.o ../utils/mytimer.mod.o | $(opari2-config --region-initialization) > pompregions_c.c

gcc -I${OPARI2_PATH}/include -c pompregions_c.c -o pompregions_c.o

g++ -O3 -fopenmp -I. -I../utils -I../fem -DMINIFE_SCALAR=double -DMINIFE_LOCAL_ORDINAL=int -DMINIFE_GLOBAL_ORDINAL=int -DMINIFE_CSR_MATRIX main.mod.o ../utils/BoxPartition.mod.o YAML_Doc.mod.o YAML_Element.mod.o ../utils/param_utils.mod.o ../utils/utils.mod.o ../utils/mytimer.mod.o  pompregions_c.o -L${LIBNINA_PATH} -lnina -lcpufreq -o miniFEcomPOMP2.x

export LD_LIBRARY_PATH=$LIBNINA_PATH
ldd miniFEcomPOMP2.x
echo "export LD_LIBRARY_PATH=$LIBNINA_PATH"

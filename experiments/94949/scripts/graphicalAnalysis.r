library(readr)
library(dplyr)

df <- read_csv("/home/gbmoro/Desktop/resultadosDaHype294949.csv")

df2 <- df %>% select(application, threads, input, version, time, cpuenergy, memenergy) %>% group_by(application, threads, input, version) %>% summarize(N=n(), time_m=mean(time), cpuenergy_m=mean(cpuenergy), memenergy_m=mean(memenergy)) %>% as.data.frame()

df2$total_energy <- df2$cpuenergy_m + df2$memenergy_m

df_backprop <- df2 %>% filter(application=='backprop')
df_kmeans <- df2 %>% filter(application=='kmeans')
df_hotspot <- df2 %>% filter(application=='hotspot')

ggplot(df_backprop, aes(x=threads, y=as.factor(as.integer(total_energy)), group=version, color=version)) + geom_point(width=1.2) + theme_bw(base_size = 14) + facet_grid(application~input) + scale_shape_identity() + ylab("Total Energy [Joules]") + xlab("Threads")
ggplot(df_kmeans, aes(x=threads, y=as.factor(as.integer(total_energy)), group=version, color=version)) + geom_point(width=1.2) + theme_bw(base_size = 14) + facet_grid(application~input) + scale_shape_identity() + ylab("Total Energy [Joules]") + xlab("Threads")
ggplot(df_hotspot, aes(x=threads, y=as.factor(as.integer(total_energy)), group=version, color=version)) + geom_point(width=1.2) + theme_bw(base_size = 14) + facet_grid(application~input) + scale_shape_identity() + ylab("Total Energy [Joules]") + xlab("Threads")

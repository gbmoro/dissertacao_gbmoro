#!/bin/bash

#kmeans execution with scorep
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/kmeans/cpu"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/kmeans/kmeans_openmp/kmeans_scorep" -i "$HOME/benchs/rodinia/data/kmeans/kdd_cup" -n 40 -k 100
echo "kmeans is executed, ipc is ready"
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/kmeans/mem"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/kmeans/kmeans_openmp/kmeans_scorep" -i "$HOME/benchs/rodinia/data/kmeans/kdd_cup" -n 40 -k 100
echo "kmeans is executed, mem is ready"

#backprop execution with scorep
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/backprop/cpu"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/backprop/backprop_scorep" 9000000
echo "backprop is executed, ipc is ready"
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/backprop/mem"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/backprop/backprop_scorep" 9000000
echo "backprop is executed, mem is ready"

#hotspot execution with scorep
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/hotspot/cpu"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/hotspot/hotspot_scorep" 64 64 1000 40 "$HOME/benchs/rodinia/data/hotspot/temp_64" "$HOME/benchs/rodinia/data/hotspot/power_64"
echo "hotspot is ready, ipc is ready"
export OMP_NUM_THREADS=40
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM
export SCOREP_EXPERIMENT_DIRECTORY="$HOME/svn/dissertacao_gbmoro/expData/94949/hotspot/mem"
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$HOME/benchs/rodinia/openmp/hotspot/hotspot_scorep" 64 64 1000 40 "$HOME/benchs/rodinia/data/hotspot/temp_64" "$HOME/benchs/rodinia/data/hotspot/power_64"
echo "hotspot is executed, mem is ready"

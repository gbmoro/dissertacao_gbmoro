#!/bin/bash
#coding: utf-8

# AUTHOR GABRIEL BRONZATTI MORO
# EXPERIMENT: 94949

benchpath="/home/gabrielbmoro/benchs/rodinia"
inputfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/94949/expDesing.csv"
calledToPerf=""
resultfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/94949/result.csv"
amIHeader=0

#zerando arquivo de saída de experimento caso exista
rm -rf "$resultfilepath"

#abrindo o canal para escrita dos resultados
for line in $( cat $inputfilepath );
do
  if [ $amIHeader -eq 0 ]; then
    echo "$line" >> "$resultfilepath"
    amIHeader=1
  else
    name=$(echo "$line" | cut -d ',' -f1)
    order=$(echo "$line" | cut -d ',' -f2)
    no=$(echo "$line" | cut -d ',' -f3)
    rp=$(echo "$line" | cut -d ',' -f4)
    threads=$(echo "$line" | cut -d ',' -f5)
    input=$(echo "$line" | cut -d ',' -f6)
    application=$(echo "$line" | cut -d ',' -f7)
    version=$(echo "$line" | cut -d ',' -f8)
    blocks=$(echo "$line" | cut -d ',' -f9)
    timeresult=""
    cpuenergyresult=""
    memenergyresult=""

    calledToApplication=""

    if [[ "$application" == *"kmeans"* ]]; then
      if [[ "$version" == "comlibnina" ]]; then
        execname="kmeansComLibNina"
      else
        execname="kmeans"
      fi

      export NINA_MAX_FREQUENCY=2300000
      export NINA_AMOUNT_OF_CPUS=40
      export NINA_CONFIG=/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/94949/kmeansconfigurationfile

      if [[ "$input" == *"small"* ]]; then
        calledToApplication="$benchpath/openmp/kmeans/kmeans_openmp/$execname -i $benchpath/data/kmeans/kdd_cup -n $threads -k 100"
      fi
      if [[ "$input" == *"medium"* ]]; then
        calledToApplication="$benchpath/openmp/kmeans/kmeans_openmp/$execname -i $benchpath/data/kmeans/204800.txt -n $threads -k 500"
      fi
      if [[ "$input" == *"large"* ]]; then
        calledToApplication="$benchpath/openmp/kmeans/kmeans_openmp/$execname -i $benchpath/data/kmeans/819200.txt -n $threads -k 420"
      fi
    fi

  if [[ "$application" == *"backprop"* ]]; then
    if [[ "$version" == "comlibnina" ]]; then
      execname="backpropComLibNina"
    else
      execname="backprop"
    fi

    export NINA_MAX_FREQUENCY=2300000
    export NINA_AMOUNT_OF_CPUS=40
    export NINA_CONFIG=/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/94949/backpropconfigurationfile

    if [[ "$input" == *"small"* ]]; then
      calledToApplication="$benchpath/openmp/backprop/$execname 9000000"
    fi
    if [[ "$input" == *"medium"* ]]; then
      calledToApplication="$benchpath/openmp/backprop/$execname 18000000"
    fi
    if [[ "$input" == *"large"* ]]; then
      calledToApplication="$benchpath/openmp/backprop/$execname 180000000"
    fi
  fi

  if [[ "$application" == *"hotspot"* ]]; then
    if [[ "$version" == "comlibnina" ]]; then
        execname="hotspotComLibNina"
    else
        execname="hotspot"
    fi

    export NINA_MAX_FREQUENCY=2300000
    export NINA_AMOUNT_OF_CPUS=40
    export NINA_CONFIG=/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/94949/hotspotconfigurationfile

    if [[ "$input" == *"small"* ]]; then
      calledToApplication="$benchpath/openmp/hotspot/$execname 64 64 1000 $threads $benchpath/data/hotspot/temp_64 $benchpath/data/hotspot/power_64"
    fi
    if [[ "$input" == *"medium"* ]]; then
      calledToApplication="$benchpath/openmp/hotspot/$execname 512 512 200000 $threads $benchpath/data/hotspot/temp_512 $benchpath/data/hotspot/power_512"
    fi
    if [[ "$input" == *"large"* ]]; then
      calledToApplication="$benchpath/openmp/hotspot/$execname 1024 1024 80000 $threads $benchpath/data/hotspot/temp_1024 $benchpath/data/hotspot/power_1024"
    fi
  fi

  export OMP_NUM_THREADS="$threads"

  calledToPerf="perf stat -a -e \"power/energy-ram/\""

  sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/gabrielbmoro/svn/libnina/src $calledToPerf $calledToApplication 2> tmp2.txt
  memenergyresult=$(cat tmp2.txt | grep -i Joules | awk ' { print $1 } ')
  rm tmp2.txt

  calledToPerf="perf stat -a -e \"power/energy-pkg/\""

  sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/gabrielbmoro/svn/libnina/src $calledToPerf $calledToApplication 2> tmp2.txt
  cpuenergyresult=$(cat tmp2.txt | grep -i Joules | awk ' { print $1 } ')
  timeresult=$(cat tmp2.txt | grep -i seconds | awk ' { print $1 } ')
  rm tmp2.txt

  echo "$name,$order,$no,$rp,$threads,$input,$application,$version,$blocks,$timeresult,$cpuenergyresult,$memenergyresult" >> "$resultfilepath"
  fi
done


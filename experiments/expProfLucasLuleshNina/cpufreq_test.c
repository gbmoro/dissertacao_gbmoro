#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <cpufreq.h>

double gettime()
{
   struct timespec s;
   clock_gettime(CLOCK_REALTIME, &s);
   double res = s.tv_sec + ((double)s.tv_nsec)/1000000000;
   return res;
}

int main(int argc, char **argv)
{
  long nanosecs = 125000000;
  if (argc == 2){
    nanosecs = atof(argv[1])*1000000;
  }
  
  double t0 = gettime();
  struct timespec r;
  struct timespec s;
  s.tv_sec = 0;
  s.tv_nsec = nanosecs;

  while (1){
    nanosleep(&s, &r);
    double t2 = gettime();
    float c0 = (float)cpufreq_get_freq_kernel(0)/1000000;
    float c1 = (float)cpufreq_get_freq_kernel(1)/1000000;
    float c2 = (float)cpufreq_get_freq_kernel(2)/1000000;
    float c3 = (float)cpufreq_get_freq_kernel(3)/1000000;
    double t3 = gettime();
    
    printf("%.9f %.2f %.2f %.2f %.2f %.6f\n",
	   gettime()-t0,
	   c0, c1, c2, c3, (t3-t2)*1000000);
  }
  return 0;
}

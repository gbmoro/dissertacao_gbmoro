#!/bin/bash

exec () {
    srcpath="/home/gabrielbmoro/benchs/miniFEcomScorep/miniFE_openmp-2.0-rc3/src"
    output="$HOME/repos/dissertacao_gbmoro/expData/2048"

    sudo cpufreq-set -g performance

    
    export OMP_NUM_THREADS="$1"
    export SCOREP_ENABLE_TRACING=true 
    export SCOREP_TOTAL_MEMORY=3G
    export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
    export OMP_PROC_BIND=FALSE 
    export GOMP_CPU_AFFINITY="0-$1"
    export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM 
    export SCOREP_EXPERIMENT_DIRECTORY="$output/scorep_package_memory" 
    
    sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$srcpath/miniFE.x"
    
    export OMP_NUM_THREADS="$1" 
    export SCOREP_ENABLE_TRACING=true 
    export SCOREP_TOTAL_MEMORY=3G
    export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
    export OMP_PROC_BIND=FALSE 
    export GOMP_CPU_AFFINITY="0-$1"
    export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC 
    export SCOREP_EXPERIMENT_DIRECTORY="$output/scorep_package_ipc" 

    sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 "$srcpath/miniFE.x"

    sudo cpufreq-set -g ondemand
}

exec "$1"


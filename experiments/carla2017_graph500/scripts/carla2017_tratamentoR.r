library(reshape);
library(dplyr);
library(tidyr);
library(readr);

read_exp_gabriel <- function (filename)
{
   # Ler o arquivo com a função read_csv do pacote readr
   df <- read.csv(filename, header=FALSE, sep=",");

   # definindo a quantidade de contadores
   hardware_counters = 4;
    
   # Configuração para a extração automática dos dados
   extra_start = 9;
   extra_total = length(names(df)) - extra_start;    
   extra_half = extra_total /2;
   extra_half = as.integer(extra_half)

   # Fazer a subtração automaticamente
   for (i in c(extra_start:(extra_start+extra_half+1))){
      x = i;
      y = i+extra_half;
      df[,x] = df[,y] - df[,x];
   }
    
   # Remover as colunas extra que não são mais necessárias
   df <- df %>% select(1:((extra_start-1)+hardware_counters));

   df <- df %>% rename(Type=V1,Thread=V2,State=V3,Start=V4,End=V5,Duration=V6,Imbrication=V7,Region=V8,L2_TCA=V9,L2_TCM=V10,TOT_INST=V11,TOT_CYC=V12);
    
   # Separar o nome do estado para obter
   # - Nome do arquivo
   # - Número da linha
    df <- df %>% separate(Region, into=c("Value", "Location"), sep=" @", convert=TRUE) %>%
        separate(Location, into=c("File", "Line"), sep=":") %>%
        mutate(State = as.factor(State),Type = as.factor(Type),Value = as.factor(Value),
               Imbrication = as.integer(Imbrication),Line = as.integer(Line));

   # Criar uma identificação mais simples das threads
  dft <- df %>% select(Thread) %>% arrange(Thread) %>%
     unique %>% mutate(ThreadId = 1:n())

  # Integrar a nova identificação no dataframe dos dados
  df <- df %>% left_join(dft, by="Thread") %>% select(-Thread) %>% rename(Thread=ThreadId);

  # Criar uma coluna Origin que associa os dados ao nome do arquivo
  df <- df %>% mutate(Origin = as.factor(filename));

  print("dataframe is ready");
    
  # Retornar o df de um único rastro
  return(df);
    
}

traces <- list.files("../expData/carla2017_graph500", pattern="csv", full.names=TRUE, recursive=TRUE);

dataframe <- do.call("rbind", lapply(traces, function(x) { read_exp_gabriel(x) }));

write.csv(dataframe, file = "../expData/carla2017_graph500/dataframegenerated.csv");

library(reshape);
library(dplyr);
library(tidyr);
library(readr);

read_exp_gabriel <- function (filename)
{
   # Ler o arquivo com a função read_csv do pacote readr
   df <- read.csv(filename, header=FALSE, sep=",");

   # definindo a quantidade de contadores
   hardware_counters = 4;
    
   # Configuração para a extração automática dos dados
   extra_start = 9;
   extra_total = length(names(df)) - extra_start;    
   extra_half = extra_total /2;
   extra_half = as.integer(extra_half)

   # Fazer a subtração automaticamente
   for (i in c(extra_start:(extra_start+extra_half+1))){
      x = i;
      y = i+extra_half;
      df[,x] = df[,y] - df[,x];
   }
    
   # Remover as colunas extra que não são mais necessárias
   df <- df %>% select(1:((extra_start-1)+hardware_counters));

   df <- df %>% rename(Type=V1,Thread=V2,State=V3,Start=V4,End=V5,Duration=V6,Imbrication=V7,Region=V8,L2_TCA=V9,L2_TCM=V10,TOT_INST=V11,TOT_CYC=V12);
    
   # Separar o nome do estado para obter
   # - Nome do arquivo
   # - Número da linha
    df <- df %>% separate(Region, into=c("Value", "Location"), sep=" @", convert=TRUE) %>%
        separate(Location, into=c("File", "Line"), sep=":") %>%
        mutate(State = as.factor(State),Type = as.factor(Type),Value = as.factor(Value),
               Imbrication = as.integer(Imbrication),Line = as.integer(Line));

   # Criar uma identificação mais simples das threads
  dft <- df %>% select(Thread) %>% arrange(Thread) %>%
     unique %>% mutate(ThreadId = 1:n())

  # Integrar a nova identificação no dataframe dos dados
  df <- df %>% left_join(dft, by="Thread") %>% select(-Thread) %>% rename(Thread=ThreadId);

  # Criar uma coluna Origin que associa os dados ao nome do arquivo
  df <- df %>% mutate(Origin = as.factor(filename));

  print("dataframe is ready");
    
  # Retornar o df de um único rastro
  return(df);
    
}

traces <- list.files("../expData/carla2017_graph500", pattern="csv", full.names=TRUE, recursive=TRUE);

dataframe <- do.call("rbind", lapply(traces, function(x) { read_exp_gabriel(x) }));

write.csv(dataframe, file = "../expData/carla2017_graph500/dataframegenerated.csv");

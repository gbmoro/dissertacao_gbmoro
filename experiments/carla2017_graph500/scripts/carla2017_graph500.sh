#!/bin/bash

programsPath="$HOME/benchs/graph500-2.1.4-energy/omp-csr"
targetPath="$HOME/repos/dissertacao_gbmoro/expData/carla2017_graph500"
scriptsPath="$HOME/repos/dissertacao_gbmoro/scripts"

sudo modprobe msr 

sudo chmod +x turbo-boost.sh
#desabilitando o turbo-boost
$scriptsPath/turbo-boost.sh disable

#setando para frequência máxima de processador
sudo cpufreq-set -g performance


x=0

while [ $x -lt 10 ]; do

    #configurando variáveis de ambiente
    export OMP_NUM_THREADS=40 
    export SCOREP_ENABLE_TRACING=true 
    export SCOREP_ENABLE_PROFILING=true 
    export SCOREP_TOTAL_MEMORY=3G 
    export SCOREP_FILTERING_FILE=/home/gabrielbmoro/repos/dissertacao_gbmoro/expData/carla2017_graph500/scorep_filter 
    export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
    export OMP_PROC_BIND=FALSE 
    export GOMP_CPU_AFFINITY=0-40 
    export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM,PAPI_TOT_INS,PAPI_TOT_CYC 
    export SCOREP_EXPERIMENT_DIRECTORY="$targetPath/repl_$x" 

    #executando programa
    sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 $programsPath/omp-csr -s 21 -e 14

    echo "--> $targetPath/repl$x"

    let x=x+1

done

$scriptsPath/turbo-boost.sh enable

#sudo cpufreq-set -g ondemand

require(DoE.base);
  lulesh28032018DOE <- fac.design (
           nfactors=2,
           replications=3,
           repeat.only=FALSE,
           blocks=1,
           randomize=TRUE,
           seed=10373,
           nlevels=c(2,9),
           factor.names=list(
                size=c(160,180),
                version=c("nina_maxstart","ondemand","performance","powersave","nina_misto","ninalibipcmajor50","ninalibmissesmajor50","ninamax","ninamin")));
  export.design(lulesh28032018DOE,
                path=".",
                filename=NULL,
                type="csv",
                replace=TRUE,
                response.names=c("energy", "time"));

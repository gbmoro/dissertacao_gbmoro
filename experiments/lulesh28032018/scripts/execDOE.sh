#!/usr/bin/env bash
#coding: utf-8

set -o errexit
set -o pipefail
#set -o nounset
set -o xtrace


#variables used with paths
homee=/home/gabrielbmoro
repo=$homee/svn/dissertacao_gbmoro
controlpath=$repo/src/controlandoUmExperimento
expdatapath=$repo/experiments/lulesh28032018/expData
appmeter=$homee/Programas/rapl-tools/AppPowerMeter
benchspath=$homee/benchs
lnina=$homee/svn/libnina/src

 	
nameDoEFile=$expdatapath/lulesh28032018DOE.csv


doeLines=$(cat $nameDoEFile)

i=0

sudo $controlpath/disable_hyperthreading.sh
sudo $controlpath/disable_turboboost.sh

for row in ${doeLines[@]}; do
	if [ $i -gt 0 ]; then
		name=$(echo "$row" | cut -d ',' -f1)
                runId=$(echo "$row" | cut -d ',' -f2)
	        runNumber=$(echo "$row" | cut -d ',' -f3)
		runStd=$(echo "$row" | cut -d ',' -f4)
		size=$(echo "$row" | cut -d ',' -f5 |  sed 's/"//g')
		version=$(echo "$row" | cut -d ',' -f6 | sed 's/"//g')
		blocks=$(echo "$row" | cut -d ',' -f7)
		energyoutput=$(echo "$row" | cut -d ',' -f8 |  sed 's/"//g')
		timeoutput=$(echo "$row" | cut -d ',' -f9 |  sed 's/"//g')

		appToExecute=""
		ldbefore=""

		if [ "$version" != "nina_maxstart" ] && [ "$version" != "nina_misto" ] && [ "$version" != "ninamax" ] && [ "$version" != "ninamin" ] && [ "$version" != "ninalibipcmajor50" ] && [ "$version" != "ninalibmissesmajor50" ]; then
			sudo cpupower frequency-set -g "$version"
			appToExecute=$(echo " $benchspath/lulesh2.0.3_omp/lulesh2.0 ")
			ldbefore=""
		else
			sudo cpupower frequency-set -g userspace
			ninaFile=$expdatapath/nina_maxstart.conf
			export NINA_AMOUNT_OF_CPUS=20
			export NINA_TARGET_CPUS=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
			export NINA_CONFIG="$ninaFile"
			unset NINA_PAPI
			appToExecute=$(echo " $benchspath/lulesh2.0.3_opari2/lulesh2.0 ")
			ldbefore=" LD_LIBRARY_PATH=$lnina "
		fi
		sudo modprobe msr
		export OMP_NUM_THREADS=20
		export OMP_PROC_BIND=true

		sudo -E $ldbefore $appmeter $appToExecute -s $size > /tmp/data.txt

		energyoutput=$(cat /tmp/data.txt | grep -i Total[[:space:]]Energy: | awk ' { print $3 } ')
		timeoutput=$(cat /tmp/data.txt | grep -i Time: | awk ' { print $2 } ')
		echo "$name,$runId,$runNumber,$runStd,$size,$version,$blocks,$energyoutput,$timeoutput" >> $expdatapath/finalResult.csv
	else
		echo "$row" >> $expdatapath/finalResult.csv
	fi
	let i=$i+1
done


sudo $controlpath/enable_turboboost.sh
sudo $controlpath/enable_hyperthreading.sh

library(readr);
library(dplyr);

df <- read_csv("../../expData/D43/result.csv", trim_ws=TRUE, col_names=TRUE);

df1 <- df %>% 
 select(metric, version, value) %>% 
 group_by(metric,version) %>% 
 summarize(count=n(), mean=mean(value), sd=sd(value), se=3*sd(value)/sqrt(n())) %>%
    as.data.frame();

df1

gain_cpuenergy <- (1 - df1$mean[1] / df1$mean[2]) * 100
gain_memenergy <- (1 - df1$mean[3] / df1$mean[4]) * 100
gain_time <- 1 - (1 - df1$mean[5] / df1$mean[6]) * 100


print("gain_cpu energy")
gain_cpuenergy
print("gain_mem energy")
gain_memenergy
print("gain_time")
gain_time


require(DoE.base);
  expDesing <- fac.design (
           nfactors=2,
           replications=40,
           repeat.only=FALSE,
           randomize=TRUE,
           seed=10373,
           nlevels=c(3,2),
           factor.names=list(
                metric=c("time","cpuenergy","memenergy"),
                version=c("comlibnina","semlibnina")));

  export.design(expDesing, path=".", filename=NULL, type="csv",replace=TRUE,
  response.names=c("value"));

#!/bin/bash
#coding: utf-8

# AUTHOR GABRIEL BRONZATTI MORO
# EXPERIMENT: D43

benchpath="/home/gabrielbmoro/benchs/miniFE_openmp-2.0-rc3/src/"
inputfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/D43/expDesing.csv"
execname=""
calledToPerf=""
params="-nx 256"
resultfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/D43/result.csv"
amIHeader=0

#zerando arquivo de saída de experimento caso exista
rm -rf "$resultfilepath"

#abrindo o canal para escrita dos resultados
for line in $( cat $inputfilepath );
do
    if [ $amIHeader -eq 0 ]; then
	echo "$line" >> "$resultfilepath"
        amIHeader=1
    else
        name=$(echo "$line" | cut -d ',' -f1)
        order=$(echo "$line" | cut -d ',' -f2)
        no=$(echo "$line" | cut -d ',' -f3)
        rp=$(echo "$line" | cut -d ',' -f4)
        metric=$(echo "$line" | cut -d ',' -f5)
        version=$(echo "$line" | cut -d ',' -f6)
        blocks=$(echo "$line" | cut -d ',' -f7)
        value=$(echo "$line" | cut -d ',' -f8)

	echo "-------------------------------------------------"
	
        calledToPerf=""

	calledToPerf=""
	
        if [[ "$metric" == *"memenergy"* ]]; then
            calledToPerf="perf stat -a -e \"power/energy-ram/\""
	fi
        if [[ "$metric" == *"cpuenergy"* ]]; then
            calledToPerf="perf stat -a -e \"power/energy-pkg/\""
	fi
        
	echo "called to perf: $calledToPerf"

        if [[ "$version" == "comlibnina" ]]; then
            execname="miniFEcomPOMP2.x"
        else
            execname="miniFE.x"
        fi

	echo "Version: $version"

	sudo chmod +x /home/gabrielbmoro/svn/libnina/scripts/setgov.sh

	sudo /home/gabrielbmoro/svn/libnina/scripts/setgov.sh
	
    	export NINA_CONFIG=/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/D43/miniFE.conf

        sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/gabrielbmoro/svn/libnina/src $calledToPerf $benchpath$execname $params > tmp.txt 2> tmp2.txt
	

        if [[ "$metric" == *"energy"* ]]; then
            value=$(cat tmp2.txt | grep -i Joules | awk ' { print $1 } ')
        else
            value=$(cat tmp.txt | grep -i making | awk ' { print $7 } ')
        fi

	echo "Metric: $metric"
	
	echo "Value: $value"

	rm tmp.txt
	rm tmp2.txt

	echo "$name,$order,$no,$rp,$metric,$version,$blocks,$value" >> "$resultfilepath"
    fi
done


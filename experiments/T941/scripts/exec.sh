#!/usr/bin/env bash
#coding: utf-8

# AUTHOR GABRIEL BRONZATTI MORO
# EXPERIMENT: T941

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

__svnpath="$HOME/svn/dissertacao_gbmoro"
__benchpath="$HOME/benchs/NPB3.3-OMP/bin"
__inputfilepath="${__svnpath}/experiments/T941/expData/expDesing.csv"
__resultfilepath="${__svnpath}/experiments/T941/expData/result.csv"
__getinfopath="${__svnpath}/experiments/T941/expData/getinfo.org"
__amIHeader=0
__ldlibrarypath="LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4"
__pcmcall="$HOME/Programs/intelpcm/pcm/pcm.x"
__scriptspath="${__svnpath}/src/controlandoUmExperimento"

#zerando arquivo de saída de experimento caso exista
#"rm -rf ${__resultfilepath}"

sudo "${__scriptspath}"/get_info.sh "${__getinfopath}"

#sudo "${__scriptspath}"/disable_hyperthreading.sh

#abrindo o canal para escrita dos resultados
for __line in $(cat "${__inputfilepath}");
do
    if [ ${__amIHeader} -eq 0 ]; then
      echo "${__line}" >> "${__resultfilepath}"
      __amIHeader=1
    else
      __name=$(echo "${__line}" | cut -d ',' -f1 | sed 's/\"//g')
      __order=$(echo "${__line}" | cut -d ',' -f2 | sed 's/\"//g')
      __no=$(echo "${__line}" | cut -d ',' -f3 | sed 's/\"//g' )
      __rp=$(echo "${__line}" | cut -d ',' -f4 | sed 's/\"//g')
      __application=$(echo "${__line}" | cut -d ',' -f5 | sed 's/\"//g')
      __governor=$(echo "${__line}" | cut -d ',' -f6 | sed 's/\"//g')
      __blocks=$(echo "${__line}" | cut -d ',' -f7 | sed 's/\"//g')
      __timeresult=""
      __cpuenergyresult=""
      __memenergyresult=""

      #configurando governor de forma adequada
      sudo cpupower frequency-set --governor "${__governor}"

      sudo "${__ldlibrarypath}" "${__pcmcall}" --noJKTWA -r --external-program "${__benchpath}"/"${__application}" >> tmp.log

      __timeresult=$(cat tmp.log | grep -i "Time[[:space:]]in[[:space:]]seconds" | awk ' { print $5 } ')
      __memenergyresult=$(cat tmp.log | awk ' { print $5 } ' | tail -n 1)
      __cpuenergyresult=$(cat tmp.log | awk ' { print $4 } ' | tail -n 1)

      sudo rm tmp.log

      echo "${__name},${__order},${__no},${__rp},${__application},${__governor},${__blocks},${__timeresult},${__cpuenergyresult},${__memenergyresult}" >> "${__resultfilepath}"
    fi
done

#sudo "${__scriptspath}"/enable_hyperthreading.sh


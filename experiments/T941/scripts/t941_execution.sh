#!/bin/bash
#coding: utf-8

# AUTHOR GABRIEL BRONZATTI MORO
# EXPERIMENT: T941

benchpath="/home/gabrielbmoro/benchs/NPB3.3-OMP/bin/"
inputfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/T941/expDesing.csv"
resultfilepath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/T941/result.csv"
getinfopath="/home/gabrielbmoro/svn/dissertacao_gbmoro/expData/T941/getinfo.org"
amIHeader=0
ld_library_path="LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4"
pcm_call="/home/gabrielbmoro/Programs/intelpcm/pcm/pcm.x"


#zerando arquivo de saída de experimento caso exista
rm -rf "$resultfilepath"

sudo /home/gabrielbmoro/svn/dissertacao_gbmoro/scripts/get_info.sh "$getinfopath"

sudo /home/gabrielbmoro/svn/dissertacao_gbmoro/scripts/controlandoUmExperimento/disable_hyperthreading.sh

#abrindo o canal para escrita dos resultados
for line in $( cat $inputfilepath );
do
    if [ $amIHeader -eq 0 ]; then
        echo "$line" >> "$resultfilepath"
        amIHeader=1
    else
        name=$(echo "$line" | cut -d ',' -f1)
        order=$(echo "$line" | cut -d ',' -f2)
        no=$(echo "$line" | cut -d ',' -f3)
        rp=$(echo "$line" | cut -d ',' -f4)
        applications=$(echo "$line" | cut -d ',' -f5)
        governor=$(echo "$line" | cut -d ',' -f6)
        Blocks=$(echo "$line" | cut -d ',' -f7)
        timeresult=""
        cpuenergyresult=""
        memenergyresult=""

	#configurando governor de forma adequada
	echo "sudo cpupower frequency-set --governor $governor"
	echo "setting to $governor"
        echo "Result: $(cpufreq-info  | grep -i "The Governor" | tail -n 1)"

        echo "-------------------------------------------------"

        echo "sudo "$pcm_call $ld_library_path" --noJKTWA -r --external-program "$benchpath$applications" >> tmp.log"

#        timeresult=$(cat tmp.log | grep -i "Time[[:space:]]in[[:space:]]seconds" | awk ' { print $5 } ')
#        memenergyresult=$(cat tmp.log | awk ' { print $5 } ' | tail -n 1)#
# 	 cpuenergyresult=$(cat tmp.log | awk ' { print $4 } ' | tail -n 1)

#        sudo rm tmp.log
	
#	echo "$name,$order,$no,$rp,$applications,$governor,$Blocks,$timeresult,$cpuenergyresult,$memenergyresult" >> "$resultfilepath"
    
	fi
done


sudo /home/gabrielbmoro/svn/dissertacao_gbmoro/scripts/controlandoUmExperimento/enable_hyperthreading.sh        

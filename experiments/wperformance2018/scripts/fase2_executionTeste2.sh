#!/usr/bin/env bash
#coding: utf-8

set -o errexit
set -o pipefail
#set -o nounset
set -o xtrace


#variables used with paths
homee=/home/gabrielbmoro
repo=$homee/svn/dissertacao_gbmoro
controlpath=$repo/src/controlandoUmExperimento
expdatapath=$repo/experiments/wperformance2018/expData
appmeter=$homee/Programas/rapl-tools/AppPowerMeter
benchspath=$homee/benchs
lnina=$homee/svn/libnina/src

declare -a mode=("nina_misto" "nina_lib_ipc_major50" "ondemand" "powersave" "performance" "ninalibmissesmajor50")
declare -a sizeUsed=("80" "90" "100" "110" "120")

executions=20

sudo $controlpath/disable_hyperthreading.sh
sudo $controlpath/disable_turboboost.sh

count=0

for s in "${sizeUsed[@]}"
do
	for m in "${mode[@]}"
	do
		appToExecute=""
		ldbefore=""
		if [ "$m" != "nina_misto" ] && [ "$m" != "nina_lib_ipc_major50" ] && [ "$m" != "ninalibmissesmajor50" ] ; then
			sudo cpupower frequency-set -g "$m"
			appToExecute=$(echo " $benchspath/lulesh2.0.3_omp/lulesh2.0 ")
			ldbefore=" "
		else
		        sudo cpupower frequency-set -g userspace
			ninaFile=""
			if [ "$m" == "nina_lib_ipc_major50" ] ; then
				ninaFile=$expdatapath/ninalibipcmajor50.conf
			fi
			if [ "$m" == "ninalibmissesmajor50" ]; then
				ninaFile=$expdatapath/ninalibmissesmajor50.conf
			fi
			if [ "$m" == "nina_misto" ]; then
				ninaFile=$expdatapath/nina_misto.conf
			fi
			export NINA_AMOUNT_OF_CPUS=20
			export NINA_TARGET_CPUS=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
			export NINA_CONFIG="$ninaFile"
			unset NINA_PAPI
			appToExecute=$(echo " $benchspath/lulesh2.0.3_opari2/lulesh2.0 ")
			ldbefore=" LD_LIBRARY_PATH=$lnina "
		fi
		energyInJoules=0
		timeInSeconds=0
		count=0
		while [ $count -lt $executions ]
		do
		        sudo modprobe msr
		        export OMP_NUM_THREADS=20
			export OMP_PROC_BIND=true
		        sudo -E $ldbefore $appmeter $appToExecute -s $s > /tmp/data.txt
			energyTmp=$(cat /tmp/data.txt | grep -i Total[[:space:]]Energy: | awk ' { print $3 } ')
			energyInJoules=$(echo "$energyInJoules + $energyTmp" | bc -l)
			timeTmp=$(cat /tmp/data.txt | grep -i Time: | awk ' { print $2 } ')
			timeInSeconds=$(echo "$timeInSeconds + $timeTmp" | bc -l)
			count=$(echo "$count+1" | bc)
		done
		energyInJoules=$(echo "$energyInJoules / $executions" | bc -l)
		timeInSeconds=$(echo "$timeInSeconds / $executions" | bc -l)
		echo "$s,$m,$energyInJoules,$timeInSeconds" >> $expdatapath/output.csv
	done
done


sudo $controlpath/enable_turboboost.sh
sudo $controlpath/enable_hyperthreading.sh

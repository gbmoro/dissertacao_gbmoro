require(DoE.base);
  wperformanceExp <- fac.design (
           nfactors=2,
           replications=5,
           repeat.only=FALSE,
           blocks=1,
           randomize=TRUE,
           seed=10373,
           nlevels=c(2,2),
           factor.names=list(
                size=c(160,180),
                version=c("nina_maxstart","ondemand")));
  export.design(wperformanceExp,
                path=".",
                filename=NULL,
                type="csv",
                replace=TRUE,
                response.names=c("energy", "time"));
library(dplyr)
library(ggplot2)
library(reshape2)

df <- read.csv("experiments/wperformance2018/expData/fase2/expermentoFase2_teste5Resultado.csv")


dfOnly <- df %>% filter(size==180)

ggplot(data=dfOnly, aes(x=time_m, y=energy_m, color=version)) + geom_point(stat="identity") + geom_errorbar(aes(ymin=energy_m-energy_se, ymax=energy_m+energy_se)) + theme_bw() + theme(text=element_text(size=12), axis.text.x=element_text(angle=90)) + labs(title="Lulesh - Tamanho 180", x="Tempo (s)", y="Energia (J)")

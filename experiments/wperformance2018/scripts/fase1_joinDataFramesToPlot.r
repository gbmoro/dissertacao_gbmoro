library(readr);
library(tidyr);
library(dplyr);
library(magrittr);
library(stringr);
library(reshape);
library(ggplot2);

df_ipc <- read.csv("../expData/fase1/libnina_ipc.csv", header=TRUE, sep=" ") %>% as.data.frame();

df_ipc <- df_ipc %>% mutate(IPC = PAPI_TOT_INS / PAPI_TOT_CYC) %>% as.data.frame();

df_ipc <- df_ipc %>% select(-PAPI_TOT_CYC, -PAPI_TOT_INS);

df_mem <- read.csv("../expData/fase1/libnina_misses.csv", header=TRUE, sep=" ");

df_mem <- df_mem %>% mutate(MISSES_L2 = PAPI_L2_TCM / PAPI_L2_TCA) %>% as.data.frame();

df_mem <- df_mem %>% select(-PAPI_L2_TCM, -PAPI_L2_TCA);

#names(df_ipc) <- c("line", "end", "start", "duration", "cyc", "ins")

df_ipc <- df_ipc %>% select(Line, IPC) %>% group_by(Line) %>% summarize(mean.IPC = mean(IPC)) %>% as.data.frame();

df_mem <- df_mem %>% select(Line, MISSES_L2) %>% group_by(Line) %>% summarize(mean.MISSES_L2 = mean(MISSES_L2)) %>% as.data.frame();

df <- merge(df_ipc, df_mem)  %>% as.data.frame();

df$se.IPC <- 0

df$se.MISSES_L2 <- 0

df <- df %>% select(Line, mean.IPC, se.IPC, mean.MISSES_L2, se.MISSES_L2) %>% filter(mean.MISSES_L2 > 0.50) %>% as.data.frame();

df$code <- seq(from=1, to=nrow(df), by = 1)

df <- df %>% select(code, Line, mean.IPC, se.IPC, mean.MISSES_L2, se.MISSES_L2) %>% as.data.frame();

#df <- df %>% melt(id = c("Line"));

#names(df) <- c("Linha", "Medida", "Valor")

write.table(df, "../expData/fase1/luleshfase1.csv", sep="\t", quote = FALSE, row.names = FALSE)

#ggplot(df, aes(x=as.factor(Linha), y=Valor, fill=Medida)) + 
#geom_bar(stat="identity", position=position_dodge()) + ylab("Porcentagem") + xlab("Linha de Código") +  
#scale_fill_manual("Medida", values = alpha(c("IPC" = "green", "MISSES_L2" = "orange"),.5)) + scale_y_reverse() + theme(axis.text.x.top = element_text(angle = 90, vjust = 0.5, hjust = 0)) + scale_x_discrete(position = "top") + expand_limits(y=0.2);

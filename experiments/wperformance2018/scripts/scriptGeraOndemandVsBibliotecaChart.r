library(dplyr)
library(ggplot2)
library(reshape2)

df <- read.csv("experiments/wperformance2018/expData/fase2/expermentoFase2_teste5Resultado.csv")

df <- df %>% select(size, Modo, time, energy) %>% group_by(size, Modo) %>% summarize(N=n(), time_m=mean(time), time_sd=sd(time), time_se=3*sd(time)/sqrt(n()), energy_m=mean(energy), energy_sd=sd(energy), energy_se=3*sd(energy)/sqrt(n())) %>% as.data.frame()


ggplot(data=df, aes(x=Modo, y=as.factor(energy_m), fill=Modo)) + geom_bar(stat="identity") + theme_bw() + facet_wrap(~size, ncol=1) + theme(text=element_text(size=12), axis.text.x=element_text(angle=90)) + coord_flip() + labs(title="Lulesh - Ondemand vs Biblioteca", x="Versão", y="Energia (J)")

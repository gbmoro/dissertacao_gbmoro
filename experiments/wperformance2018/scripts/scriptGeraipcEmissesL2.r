library(dplyr)
library(ggplot2)
library(reshape2)

df <- read.csv("experiments/wperformance2018/expData/fase1/luleshbehaviorfase1.csv")

df <- df %>% melt(id=c("code", "Line"))

names(df) <- c("id", "Linha", "Medida", "Valor")

ggplot(df,  aes(x=as.factor(Linha), y=Valor, color=Medida)) + geom_point(position=position_dodge(0.2), stat="identity") + geom_line(aes(group=Medida)) + theme(text= element_text(size=12), axis.text.x=element_text(angle=90)) + labs(title="Mapeamento de Medidas - Lulesh", x="Linha de Código", y="Valor (%)")

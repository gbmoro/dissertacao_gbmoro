#!/bin/bash

programsPath="$HOME/benchs/graph500-2.1.4-energy/omp-csr"
targetPath="/media/gabrielbmoro/obiwan/repos/dissertacao_gbmoro/expData/296"
scriptsPath="/media/gabrielbmoro/obiwan/repos/dissertacao_gbmoro/scripts"

#sudo modprobe msr 
sudo chmod +x $scriptPath/turbo-boost.sh

echo "Desabilitando turbo-boost"
#desabilitando o turbo-boost
$scriptsPath/turbo-boost.sh disable

echo "Configurando para a frequência máxima de processador"
#setando para frequência máxima de processador
sudo cpufreq-set -g performance

echo "Definindo variáveis do scorep"
#configurando variáveis de ambiente
export OMP_NUM_THREADS=4 
export SCOREP_ENABLE_TRACING=true 
export SCOREP_ENABLE_PROFILING=true 
export SCOREP_TOTAL_MEMORY=3G 
export SCOREP_FILTERING_FILE="$targetPath/scorep_filter" 
export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
export OMP_PROC_BIND=FALSE 
export GOMP_CPU_AFFINITY=0-4
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM,PAPI_TOT_INS,PAPI_TOT_CYC 
export SCOREP_EXPERIMENT_DIRECTORY="$targetPath/scorep_package" 

echo "Executando benchmark"
#executando programa
sudo -E $programsPath/omp-csr

echo "Habilitando turbo-boost"
$scriptsPath/turbo-boost.sh enable
sudo cpufreq-set -g ondemand

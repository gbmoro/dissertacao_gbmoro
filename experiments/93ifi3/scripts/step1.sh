#coding: utf-8

# AUTHOR GABRIEL BRONZATTI MORO
# EXPERIMENT: 1031014 - STEP 1

set -o errexit
# set -o pipefail
set -o nounset
set -o xtrace

__green="\033[1;92m"
__red="\033[1;31m"
__closecoloruse="\033[0m"
__minFrequency=80000

sudo /home/gabrielbmoro/svn/dissertacao_gbmoro/src/controlandoUmExperimento/disable_hyperthreading.sh

__benchpath="$HOME/benchs/luleshByGitHUB/LULESH"
__expdatapath="$HOME"/svn/dissertacao_gbmoro/experiments/93ifi3/expData

sudo rm -rf "${__expdatapath}"
mkdir "${__expdatapath}"

export SCOREP_ENABLE_TRACING=true
export OMP_NUM_THREADS=20
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC
export SCOREP_EXPERIMENT_DIRECTORY="${__expdatapath}"/cpu

__currentpath=$(pwd)

cd "${__benchpath}"

sudo -E ./lulesh2.0 -s 120

export SCOREP_ENABLE_TRACING=true
export OMP_NUM_THREADS=20
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM
export SCOREP_EXPERIMENT_DIRECTORY="${__expdatapath}"/mem

sudo -E ./lulesh2.0 -s 120

cd "$__currentpath"

sudo chmod 777 -R "${__expdatapath}"

if [ $(ls "${__expdatapath}"/cpu/ | grep -i traces.otf2 | wc -l) -gt 0 ]; then
  otf22csv "${__expdatapath}"/cpu/traces.otf2 >> "${__expdatapath}"/cpu/traces.csv
else
  echo -e "${__red} Problems occur in otf2 execution <- cpu ${__closecoloruse}"
  exit 0
fi
if [ $(ls "${__expdatapath}"/mem/ | grep -i traces.otf2 | wc -l) -gt 0 ]; then
  otf22csv "${__expdatapath}"/mem/traces.otf2 >> "${__expdatapath}"/mem/traces.csv
else
  echo -e "${__red} Problems occur in otf2 execution <- mem ${__closecoloruse}"
  exit 0
fi

__tracelinescpu=$(cat "${__expdatapath}"/cpu/traces.csv | wc -l)
__tracelinesmem=$(cat "${__expdatapath}"/mem/traces.csv | wc -l)

if [ ${__tracelinescpu} -lt 1 ]; then
  echo -e "${__red} Problems occur in otf2->csv generation <- cpu ${__closecoloruse}"
  exit 0
fi

if [ ${__tracelinesmem} -lt 1 ]; then
  echo -e "${__red} Problems occur in otf2->csv generation <- mem ${__closecoloruse}"
  exit 0
fi

if [ ${__tracelinescpu} -eq ${__tracelinesmem} ]; then
  echo -e "${__green} The trace generated using otf22csv was executed with sucess... ${__closecoloruse}"
  Rscript step1_aux.r "${__expdatapath}"
else
  echo -e "${__red} The trace generated using otf22csv was executed with unsucess... ${__closecoloruse}"
  exit 0
fi

__tracelines=$(cat "${__expdatapath}"/configurationFile.csv | wc -l)

if [ ${__tracelines} -gt 0 ]; then
  cat "${__expdatapath}"/configurationFile.csv | sed "s/TRUE/${__minFrequency}/g" | sed '/FALSE/d' | cut  -d ',' -f2,3,5 | sed 's/\"//g' >> "${__expdatapath}"/configFile.csv
else
  echo -e "${__red} Configurationfile doesn't exists...${__closecoloruse}"
  exit 0
fi

sudo /home/gabrielbmoro/svn/dissertacao_gbmoro/src/controlandoUmExperimento/enable_hyperthreading.sh


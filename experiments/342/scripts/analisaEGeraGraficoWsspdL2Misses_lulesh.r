
 library(readr);
 library(tidyr);
 library(dplyr);
 csvToDataframe = function(path) {
     df <- read_csv(path, trim_ws=TRUE, progress=TRUE);
     
     #configurando para a extração automática dos dados
     extra_start = 9;
     extra_total = length(names(df)) - 8;
     extra_half = extra_total/2;
     
     # Fazer a subtração automaticamente
     for (i in c(extra_start:(extra_start+extra_half-1))){
         x = i;
         y = i+extra_half;
         df[,x] = df[,y] - df[,x];
     }
     
     # Remover as colunas extra que não são mais necessárias
     df <- df %>% select(1:(extra_start+extra_half-1));
     
     # Separar o nome do estado para obter
     # - Nome do arquivo
     # - Número da linha
     df <- df %>% rename(Thread = Container) %>% separate(Value, into=c("Value", "Location"), sep=" @", convert=TRUE) %>% separate(Location, into=c("File", "Line"), sep=":") %>% mutate(State = as.factor(State), Type = as.factor(Type), Value = as.factor(Value), Imbrication = as.integer(Imbrication), Line = as.integer(Line));
     
     # Criar uma identificação mais simples das threads
     dft <- df %>%  select(Thread) %>%  arrange(Thread) %>%  unique %>% mutate(ThreadId = 1:n())
     
     # Integrar a nova identificação no dataframe dos dados
     df <- df %>% left_join(dft, by="Thread") %>%  select(-Thread) %>% rename(Thread=ThreadId);
     
     # Criar uma coluna Origin que associa os dados ao nome do arquivo
     df <- df %>% mutate(Origin = as.factor("traces.csv"));
     
     rm(dft)
     
     return(df);
 }
df <- csvToDataframe("/Users/Gabriel Bronzatti Mo/Downloads/traces_memory.csv")

df1 <- df %>% mutate(misses = PAPI_L2_TCM/PAPI_L2_TCA) %>% as.data.frame()

df2 <- df1 %>% select(State,Start,State,Type,Value,File,Line,Imbrication,Thread,misses) 
%>% group_by(State,Type,Value,File,Line,Thread) %>% summarize(Start=min(Start),misses=mean(misses))


ggplot(df2, aes(x=Start,y=misses)) + geom_bar(stat="identity", color="blue", fill="blue",width=2.1) + 
theme_bw() + coord_flip() + labs(x="Runtime[s]", y="L2 Misses (%)") + facet_wrap(~file~line)

df3 <- df2 %>% filter(misses > 0.35)

plot <- ggplot(df3, aes(x=Start, y=misses, group=Line)) + geom_point(aes(color=factor(Line)),size=3) + facet_wrap(~Value)

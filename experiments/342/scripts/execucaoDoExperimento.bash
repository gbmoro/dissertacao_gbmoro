#!/bin/bash

programsPath="$HOME/benchs/lulesh_scorep"
targetPath="$HOME/repos/dissertacao_gbmoro/expData/342"
scriptsPath="$HOME/repos/dissertacao_gbmoro/scripts"

#sudo modprobe msr 
sudo chmod +x $scriptPath/turbo-boost.sh

echo "Desabilitando turbo-boost"
#desabilitando o turbo-boost
$scriptsPath/turbo-boost.sh disable

echo "Configurando para a frequência máxima de processador"
#setando para frequência máxima de processador
sudo cpufreq-set -g performance

echo "Definindo variáveis do scorep"
#configurando variáveis de ambiente
export OMP_NUM_THREADS=40 
export SCOREP_ENABLE_TRACING=true 
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_FILTERING_FILE="$targetPath/scorep_filter" 
export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
export OMP_PROC_BIND=FALSE 
export GOMP_CPU_AFFINITY=0-40
export SCOREP_METRIC_PAPI=PAPI_L2_TCA,PAPI_L2_TCM 
export SCOREP_EXPERIMENT_DIRECTORY="$targetPath/scorep_package_memory" 

echo "Executando benchmark - metrica memory"
#executando programa
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 $programsPath/lulesh2.0 -s 15 -i 100 

echo "Definindo variáveis do scorep"
#configurando variáveis de ambiente
export OMP_NUM_THREADS=40 
export SCOREP_ENABLE_TRACING=true 
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_FILTERING_FILE="$targetPath/scorep_filter" 
export SCOREP_METRIC_RUSAGE=ru_utime,ru_stime 
export OMP_PROC_BIND=FALSE 
export GOMP_CPU_AFFINITY=0-40
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_TOT_CYC 
export SCOREP_EXPERIMENT_DIRECTORY="$targetPath/scorep_package_ipc" 

echo "Executando benchmark - metrica ipc"
#executando programa
sudo -E LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4 $programsPath/lulesh2.0 -s 30 -i 500 


echo "Habilitando turbo-boost"
$scriptsPath/turbo-boost.sh enable
sudo cpufreq-set -g ondemand

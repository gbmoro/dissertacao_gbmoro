library(readr);
library(tidyr);
library(dplyr);
library(magrittr);

csvToDataframe = function(path) {
    df <- read_csv(path, trim_ws=TRUE, progress=TRUE);
	
    #configurando para a extração automática dos dados
    extra_start = 9;
    extra_total = length(names(df)) - 8;
    extra_half = extra_total/2;

    # Fazer a subtração automaticamente
    for (i in c(extra_start:(extra_start+extra_half-1))){
        x = i;
        y = i+extra_half;
        df[,x] = df[,y] - df[,x];
    }

    # Remover as colunas extra que não são mais necessárias
    df <- df %>% select(1:(extra_start+extra_half-1));

    # Separar o nome do estado para obter
    # - Nome do arquivo
    # - Número da linha
    df <- df %>% rename(Thread = Container) %>% separate(Value, into=c("Value", "Location"), sep=" @", convert=TRUE) %>% separate(Location, into=c("File", "Line"), sep=":") %>% mutate(State = as.factor(State), Type = as.factor(Type), Value = as.factor(Value), Imbrication = as.integer(Imbrication), Line = as.integer(Line));

    # Criar uma identificação mais simples das threads
    dft <- df %>%  select(Thread) %>%  arrange(Thread) %>%  unique %>% mutate(ThreadId = 1:n())

    # Integrar a nova identificação no dataframe dos dados
    df <- df %>% left_join(dft, by="Thread") %>%  select(-Thread) %>% rename(Thread=ThreadId);

    # Criar uma coluna Origin que associa os dados ao nome do arquivo
    df <- df %>% mutate(Origin = as.factor("traces.csv"));

    rm(dft)

    return(df);
}

unionOfDataframes = function(df1_ipc,df2_misses){

	#eliminando o que não interessa para o arquivo de configuração
	df1_ipc <- df1_ipc %>%
	 select(Value,File,Line,PAPI_TOT_INS,PAPI_TOT_CYC,Thread) %>% as.data.frame();

	df2_misses <- df2_misses %>%
	 select(Value,File,Line,PAPI_L2_TCA,PAPI_L2_TCM,Thread) %>% as.data.frame();

	#criar um duas colunas comuns aos data.frames
	df1_ipc <- df1_ipc %>% mutate(metricvalue=PAPI_TOT_INS/PAPI_TOT_CYC) %>% as.data.frame();
	df1_ipc <- df1_ipc %>% select(-PAPI_TOT_INS,-PAPI_TOT_CYC);
	df1_ipc$metrictype = "ipc"

	df2_misses <- df2_misses %>% mutate(metricvalue=PAPI_L2_TCM/PAPI_L2_TCA) %>% as.data.frame();
	df2_misses <- df2_misses %>% select(-PAPI_L2_TCM,-PAPI_L2_TCA);
	df2_misses$metrictype = "misses"

	#juntando tudo no dfT
	dfT <- rbind(df1_ipc,df2_misses)

	#organizando e agrupando, fazendo a media entre threads
	dfT <- dfT %>% select(Value,File,Line,metrictype,metricvalue) %>% 
	group_by(Value,File,Line,metrictype) %>%
	 summarize(metricvalue=mean(metricvalue)) %>% as.data.frame();

    return(dfT);
}

df_ipcMetric <- csvToDataframe("/home/gabrielbmoro/Documents/342/scorep_package_ipc/traces_500l.csv")

df_missesMetric <- csvToDataframe("/home/gabrielbmoro/Documents/342/scorep_package_memory/traces_500l.csv")

dfT <- unionOfDataframes(df_ipcMetric, df_missesMetric);

write.csv(dfT, file = "dfT.csv")

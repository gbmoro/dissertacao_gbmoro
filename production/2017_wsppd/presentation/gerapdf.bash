#!/bin/bash

#primeiro gera o beamer .tex via org e depois é só compilar com
#esse script

pdflatex wsppd2017.tex

rm wsppd2017.log
rm wsppd2017.nav
rm wsppd2017.out
rm wsppd2017.snm
rm wsppd2017.toc
rm wsppd2017.aux

# LEIAME #

Esse repositório possui passo a passo, a metodologia científica elaborada da dissertação de Gabriel B Moro.

### O que posso fazer para replicar a metodologia e todos os passos da pesquisa? ###

É necessário uma configuração do ambiente com as seguintes ferramentas:

* emacs24 (>24.3) 
* org-mode > 8.2
* ess
* R
* ggplot2, dplyr, reshape e DoE.base (pacotes R)
* texlive
* auctex

Quanto a instalação e configuração do org, visitar: [http://mescal.imag.fr/membres/arnaud.legrand/misc/init.php]

### Estrutura do repositório ###

* expData - contém pequenos rastros e demais arquivos de log
* img - contém gráficos e demais imagens
* scripts - contém códigos-fonte
* .org - são arquivos escritos utilizando org, os quais estão na raíz do repositório, basicamente existirá dois, um do professor Lucas e outro meu

### Com quem posso conversar ou entrar em contato para saber mais sobre a pesquisa? ###

* Aluno: Gabriel Bronzatti Moro - gbmoro@inf.ufrgs.br
* Orientador: Professor Lucas M. Schnorr - schnorr@inf.ufrgs.br
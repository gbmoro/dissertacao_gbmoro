#!/bin/bash
DIR=$(dirname $0)

#First, enable all cores
for PU in `find /sys/devices/system/cpu/ |grep cpu[0-9]*$`; do
   echo "Enabling $PU now."
    sudo zsh -c "echo 1 > ${PU}/online"
done

echo "The number of PUs now is $(hwloc-ls  --only PU | wc -l)."

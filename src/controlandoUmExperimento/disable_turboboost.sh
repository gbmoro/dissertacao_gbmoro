#!/bin/bash
DIR=$(dirname $0)

if [ `lsmod | grep msr | wc -l` -ne 1 ]; then
    echo "The =msr= module is not loaded. It should be."
    exit 1;
fi

# Get the list of online cores
ONLINECPUS=$(for CPU in $(find /sys/devices/system/cpu/ | grep -v cpu0 | grep cpu[0-9]*$); do [[ $(cat $CPU/online) -eq 1 ]] && echo $CPU; done | grep cpu[0-9]*$ | sed 's/.*cpu//')

# Enable
for PU in ${ONLINECPUS}; do
    sudo zsh -c "/usr/sbin/wrmsr -p${PU} 0x1a0 0x850089"
done

# Disable & Check
for PU in ${ONLINECPUS}; do
    echo "Disabling turbo boost mode for PU $PU."
    sudo zsh -c "/usr/sbin/wrmsr -p${PU} 0x1a0 0x4000850089"
    TURBOBOOST=$(sudo zsh -c "/usr/sbin/rdmsr -p${PU} 0x1a0 -f 38:38")
    if [[ "0" = $TURBOBOOST ]]; then
       echo "Failed to disable turbo boost for PU number $cpu. Aborting."
       exit 1
    fi
done

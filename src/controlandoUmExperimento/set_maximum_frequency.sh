#!/bin/bash
DIR=$(dirname $0)

MAXFREQ=$(cpufreq-info | grep limits | sed -e "s/.*- //" -e "s/ //g" | uniq)

# Get all online cores
ONLINECPUS=$(for CPU in $(find /sys/devices/system/cpu/ | grep -v cpu0 | grep cpu[0-9]*$); do [[ $(cat $CPU/online) -eq 1 ]] && echo $CPU; done | grep cpu[0-9]*$ | sed 's/.*cpu//')

# Core 0 is always online
ONLINECPUS="0 ${ONLINECPUS}"

for PU in ${ONLINECPUS}; do
    echo "Setting the frequency of PU ${PU} to ${MAXFREQ}"
    sudo cpufreq-set -c ${PU} -f ${MAXFREQ}
    echo "After setting to max, the frequency is now $(cat /sys/devices/system/cpu/cpu${PU}/cpufreq/scaling_cur_freq)"
done

#!/bin/bash
CPUFILE=/proc/cpuinfo
test -f $CPUFILE || exit 1
NUMPHYCPU=`grep "physical id" $CPUFILE | sort -u | wc -l`
NUMLOGCORE=`grep "processor" $CPUFILE | wc -l`
NUMPHYCORE=`grep "core id" $CPUFILE | sort -u | wc -l`
TOTALNUMPHYCORE=$(echo "$NUMPHYCPU * $NUMPHYCORE" | bc)
MODEL=`grep "model name" $CPUFILE | sort -u | cut -d : -f 2- | sed "s/^[[:space:]]*//"`
echo "This system has $NUMPHYCPU CPUs, of model \"$MODEL\"."
echo "Each physical CPU is equipped with $NUMPHYCORE physical cores (total is $TOTALNUMPHYCORE)."
if [ $TOTALNUMPHYCORE -ne $NUMLOGCORE ]; then
   echo "Hyperthreading is ON. So, there are $NUMLOGCORE logical cores."
else
   echo "Hyperthreading is OFF."
fi
exit

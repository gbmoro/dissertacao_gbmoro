#!/bin/bash
DIR=$(dirname $0)

function usage()
{
    echo "$0 [dest]";
    echo "  where <dest> is a directory where captured files should be moved to";
}

##############################
# Parameter handling section #
##############################
DEST=${1:-}
if [ -z "$DEST" ]; then
    DEST="."
fi

##############################
# For this host             #
##############################
HOST=$(hostname)

echo "** $HOST"

echo "*** Check presence of cpufreq driver"
PRESENT=$(eval "$DIR/detect_acpidriver.sh ; echo $?")
if [ $PRESENT -ne 0 ]; then
    echo "The driver acpi-cpufreq is not available in ${HOST}"
    exit
else
    echo "I believe the ${HOST} is using the acpi-cpufreq DVFS driver."
fi

echo "*** Disable hyperthreading"
sudo $DIR/disable_hyperthreading.sh

echo "*** Disable turboboost"
sudo $DIR/disable_turboboost.sh

echo "*** Set the frequency to maximum"
sudo $DIR/set_maximum_frequency.sh

echo "*** Call the get_info.sh"
GETINFO=$(echo ${HOST} | sed "s/\./_/g" | sed "s/$/_get_info.org/")
sudo $DIR/get_info.sh -t 'Get information from the ${HOST}' ${GETINFO}
echo $GETINFO
if [ "$DEST" != "." ]; then
   mv -f $GETINFO $DEST
fi


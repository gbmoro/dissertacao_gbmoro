#!/bin/bash
set -euo pipefail

# Error function
function error_script {
    ERRORMSG=${1:-}
    echo "Error: $ERRORMSG"
    exit 1
}

# Check functions
function check_var {
    VAR=${1:-}
    MSG=${2:-}
    if [[ -z "$VAR" ]]; then
        error_script $MSG
    fi
}

function check_bin {
    BIN=${1:-}
    MSG=${2:-}
    if [[ ! -x "$BIN" ]]; then
        error_script $MSG
    fi
}

function check_env {
    ENV=${1:-}
    MSG=${2:-}
    if [ -z ${ENV+x} ]; then
        error_script $MSG
    fi
}

function info {
    INFOMSG=${1:-}
    echo "Info: $INFOMSG"
}

#!/bin/bash

# fail often, so we are robust
set -euo pipefail

# source functions
DIR=$(dirname $0)
source $DIR/colors_error_report.sh

# Function to cat a file within an example block
function org_example_file {
    FILE=${1:-}
    if [ -e "$FILE" ]; then
      echo "#+BEGIN_EXAMPLE"
      cat $FILE
      echo "#+END_EXAMPLE"
	  fi
}

# Function that returns the unique org filename given an experiment unique identifier
function org_err_file {
  UNIQUE=${1:-}
  if [ -z "$UNIQUE" ]; then
    error_script "Error: Unique key not passed as parameter to org_err_file."
  fi
  echo $(basename $UNIQUE .org)_err.org
}

# Function that returns the unique org filename given an experiment unique identifier
function org_file {
  UNIQUE=${1:-}
  if [ -z "$UNIQUE" ]; then
    error_script "Error: Unique key not passed as parameter to org_file."
  fi
  echo $(basename $UNIQUE .org).org
}

function org_info_file {
  UNIQUE=${1:-}
  if [ -z "$UNIQUE" ]; then
    error_script "Error: Unique key not passed as parameter to org_file."
  fi
  echo $(basename $UNIQUE .org)_get_info.org
}

# Function that returns the unique directory name given an experiment unique identifier
function dir_file {
  UNIQUE=${1:-}
  if [ -z "$UNIQUE" ]; then
    error_script "Error: Unique key not passed as parameter to dir_file."
  fi
  echo $(basename $UNIQUE .org).dir
}

function create_experiment_dir {
  UNIQUE=${1:-}
  
  if [ -z "$UNIQUE" ]; then
    error_script "Error: Unique key not passed as parameter to create_experiment_directory."
  fi

  # Preparation of the output (org and dir)
  OUTPUTORG=$(org_file $UNIQUE)
  OUTPUTORGDIR=$(dir_file $UNIQUE)
  if [ -e $OUTPUTORG ]; then
     error_script "$OUTPUTORG already exists. Please, remove it or use another unique name."
     usage;
     exit;
  else
     info "Experiment Org file: $OUTPUTORG"
  fi
  if [ -d $OUTPUTORGDIR ]; then
     error_script "$OUTPUTORGDIR directory already exists. Please, remove it or use another unique name."
     usage;
     exit;
  else
     info "Experiment Directory: $OUTPUTORGDIR"
  fi
  # Everything seems okay, let's prepare the terrain
  touch $OUTPUTORG
  if [ $? -ne 0 ]; then
     error_script "Creation of $OUTPUTORG did not work."
     exit;
  fi
  mkdir $OUTPUTORGDIR
  if [ $? -ne 0 ]; then
     error_script "Creation of $OUTPUTORGDIR did not work."
     exit;
  fi
}

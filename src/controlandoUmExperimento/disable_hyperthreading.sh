#!/bin/bash
DIR=$(dirname $0)

#First, enable all cores
for PU in `find /sys/devices/system/cpu/ |grep cpu[0-9]*$`; do
   echo "Enabling $PU now."
    sudo zsh -c "echo 1 > ${PU}/online"
done

HYPERTHREADING=`$DIR/hyperthreading.sh | grep -e "Hyperthreading is ON" | wc -l`
if [ $HYPERTHREADING -eq 0 ]; then
   echo "Hyperthreading is OFF, so disabling is not necessary."
   exit
else
    echo "Hyperthreading is ON."
fi
echo "The number of PUs now is $(hwloc-ls  --only PU | wc -l)."
echo "I will disable hyperthreading now."
# Disable hyperthreading
# Only run this if you are sure
# - Hyperthreading is enabled
# - Each physical core has two processing units (PU)
# - hwloc-ls is installed and reports two PU per physical core
for PU in `hwloc-ls --only PU | cat -n | grep -e "[[:space:]]*[0-9]*[02468][[:space:]]*PU" | sed -e "s/^[^(]*(P#\\([0-9]*\))/\1/"`; do
   echo "Disabling PU $PU now."
   sudo zsh -c "echo 0 > /sys/devices/system/cpu/cpu${PU}/online"
done
echo "The number of PUs now is $(hwloc-ls  --only PU | wc -l)."

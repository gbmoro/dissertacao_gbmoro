#!/bin/bash
DIR=$(dirname $0)

function usage()
{
    echo "Input: number of CPUs to be used"
    echo "Output: core identifiers (NUMA-aware)"
    echo "$0 <ncpus>";
}

PRESENT=$(cpufreq-info | grep driver | uniq | grep cpufreq | wc -l)
if [ $PRESENT -ne 1 ]; then
    exit 1;
fi

exit 0

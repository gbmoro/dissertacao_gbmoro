/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step;

/**
 *
 * @author gbmoro
 */
public class ExperimentController {

    protected String m_strConfigFile;
    protected String m_strParams;
    protected String m_strExecName;
    protected String m_strExecNameWithLibNina;
    protected String m_strPathResult;

    public ExperimentController(String a_strConfigFile,
            String a_strParams,
            String a_strExecName,
            String a_strExecNameWithLibNINA,
            String a_strPathResult) {
        this.m_strConfigFile = a_strConfigFile;
        this.m_strParams = a_strParams;
        this.m_strExecName = a_strExecName;
        this.m_strExecNameWithLibNina = a_strExecNameWithLibNINA;
        this.m_strPathResult = a_strPathResult;
    }

    public void runExperiment(Object object) {
    }

    public String getConfigFile() {
        return this.m_strConfigFile;
    }

    public String getParams() {
        return this.m_strParams;
    }

    public String getExecName() {
        return this.m_strExecName;
    }

    public String getExecNameWithLibNINA() {
        return this.m_strExecNameWithLibNina;
    }

    public String getPathResult() {
        return this.m_strPathResult;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step.step_1;

import step.ExperimentController;

/**
 *
 * @author gbmoro
 */
public class ExperimentControllerStep1 extends ExperimentController{

    private final String m_strOtf2csvPath;
    private final String m_strScorepPath;
    
    public ExperimentControllerStep1(
            String a_strScorepPath,
            String a_strOtf2CsvPath,
            String a_strConfigFile, 
            String a_strParams, 
            String a_strExecName, 
            String a_strExecNameWithLibNINA, 
            String a_strPathResult) {
        super(a_strConfigFile, a_strParams, a_strExecName, a_strExecNameWithLibNINA, a_strPathResult);
        this.m_strOtf2csvPath = a_strOtf2CsvPath;
        this.m_strScorepPath = a_strScorepPath;
    }

    @Override
    public void runExperiment(Object object) {
        super.runExperiment(object); //To change body of generated methods, choose Tools | Templates.
    }
    
}
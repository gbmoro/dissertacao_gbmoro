/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step.step_1;

import step.ExecutionModel;

/**
 *
 * @author gbmoro
 */
public class Step1Model extends ExecutionModel {

    public static final String IPC_HW = "PAPI_TOT_INS,PAPI_TOT_CYC";
    public static final String MISSES_HW = "PAPI_L2_TCA,PAPI_L2_TCM";
    public static final boolean ENABLE_TRACING = true;
    public String m_strExperimentDirectory;

    public Step1Model(int a_nThreadsNumber, String a_strExperimentDirectory) {
        this.m_nThreadsNumber = a_nThreadsNumber;
        this.m_strExperimentDirectory = a_strExperimentDirectory;
    }

}

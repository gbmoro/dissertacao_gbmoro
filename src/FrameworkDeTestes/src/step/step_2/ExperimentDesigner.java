/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step.step_2;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author gbmoro
 */
public class ExperimentDesigner {

    private final ArrayList<Step2Model> m_lstExecutionModel;
    private final int m_nExecutionsNumber;
    private final ArrayList<Integer> m_lstThreadsUsed;

    public ExperimentDesigner(int a_nAmountOfThreads, int a_nExecutionsNumber, int a_nStepNumber) {
        m_lstExecutionModel = new ArrayList<Step2Model>();
        m_lstThreadsUsed = new ArrayList<Integer>();
        m_nExecutionsNumber = a_nExecutionsNumber;
        settingThreadsList(a_nAmountOfThreads, a_nStepNumber);
        settingExecutionModelList();
        randomExecutionItens();
    }

    private void addExecutionModel(Step2Model a_emExecutionModel) {
        m_lstExecutionModel.add(a_emExecutionModel);
    }

    private void randomExecutionItens() {
        Collections.shuffle(m_lstExecutionModel);
    }

    private void settingThreadsList(int a_nTotalNumberOfThreads, int a_nStep) {
        int nTotalThreadsNumberTmp = a_nTotalNumberOfThreads;

        while (nTotalThreadsNumberTmp > 0) {
            m_lstThreadsUsed.add(nTotalThreadsNumberTmp);
            nTotalThreadsNumberTmp -= a_nStep;
        }
    }

    private void settingExecutionModelList() {

        int nCount = 0, nExecutionsNumber = m_nExecutionsNumber * m_lstThreadsUsed.size() * 2;

        while (nCount < nExecutionsNumber) {
            for (int nThreadTarget : m_lstThreadsUsed) {
                m_lstExecutionModel.add(
                        new Step2Model(nThreadTarget, true)
                );
                m_lstExecutionModel.add(
                        new Step2Model(nThreadTarget, false)
                );
            }
            nCount += 2;
        }
    }

    public ArrayList<Step2Model> getExecutionModelList() {
        return m_lstExecutionModel;
    }

}

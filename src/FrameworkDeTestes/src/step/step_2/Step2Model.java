/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step.step_2;

import step.ExecutionModel;

/**
 *
 * @author gbmoro
 */
public class Step2Model extends ExecutionModel{

    public final boolean m_bIsItWithLibNina;
    public double m_sRuntime = 0f;
    public double m_sMemenergy = 0f;
    public double m_sCpuenergy = 0f;

    public Step2Model(int a_nAmountOfThreads, boolean a_bIsItWithLibNina) {
        super.m_nThreadsNumber = a_nAmountOfThreads;
        m_bIsItWithLibNina = a_bIsItWithLibNina;
    }

    @Override
    public String toString() {
        return String.valueOf("threads:" + super.m_nThreadsNumber + ","
                + "m_bIsItWithLibNina:" + m_bIsItWithLibNina + ","
                + "runtime: " + m_sRuntime + ","
                + "memenergy: " + m_sMemenergy + ","
                + "cpuenergy: " + m_sCpuenergy
        );
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package step.step_2;

import service.RuntimeService;
import service.RuntimeOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import step.ExperimentController;

/**
 *
 * @author gbmoro
 */
public class ExperimentControllerStep2 extends ExperimentController {

    private final Long m_nMaxFrequency;
    private final Long m_nMinFrequency;
    private final String m_strLibNinaPath;
    static final String MESSAGE_PROBLEM = "There was a problem when you ran the base command...";
    static final Long TIME_LIMIT_TO_MEASURE_RUNTIME = 180000l;

    public ExperimentControllerStep2(
            Long a_nMaxFrequency,
            Long a_nMinFrequency,
            String a_strConfigFile,
            String a_strLibNinaPath,
            String a_strExecName,
            String a_strExecNameWithLibNina,
            String a_strPathResult,
            String a_strParams) {
        super(a_strConfigFile,
                a_strParams,
                a_strExecName,
                a_strExecNameWithLibNina,
                a_strPathResult
        );
        this.m_nMaxFrequency = a_nMaxFrequency;
        this.m_nMinFrequency = a_nMinFrequency;
        this.m_strLibNinaPath = a_strLibNinaPath;
        this.m_strExecNameWithLibNina = a_strExecNameWithLibNina;
    }

    @Override
    public void runExperiment(Object a_objExecutionModel) {

        if (a_objExecutionModel instanceof ArrayList) {

            ArrayList<Step2Model> lstExecutionModel = (ArrayList<Step2Model>) a_objExecutionModel;

            try {
                for (Step2Model execModel : lstExecutionModel) {
                    ProcessBuilder pb = new ProcessBuilder();
                    Map<String, String> envMap = pb.environment();
                    ArrayList<String> lstCommands = new ArrayList<String>();
                    String execNameTmp;
                    if (execModel.m_bIsItWithLibNina) {
                        envMap.put("NINA_MAX_FREQUENCY", String.valueOf(m_nMaxFrequency));
                        envMap.put("NINA_AMOUNT_OF_CPUS", String.valueOf(execModel.getThreadsNumber()));
                        envMap.put("NINA_CONFIG", String.valueOf(m_strConfigFile));
                        envMap.put("LD_LIBRARY_PATH",
                                m_strLibNinaPath + ":" + envMap.get("LD_LIBRARY_PATH")
                        );
                        execNameTmp = m_strExecNameWithLibNina;
                    } else {
                        envMap.put("OMP_NUM_THREADS", String.valueOf(execModel.getThreadsNumber()));
                        execNameTmp = m_strExecName;
                    }

                    lstCommands.add("perf");
                    lstCommands.add("stat");
                    lstCommands.add("-a");
                    lstCommands.add("-e");
                    lstCommands.add("\"power/energy-ram/\"");
                    lstCommands.add(execNameTmp);
                    if (!m_strParams.isEmpty()) {
                        lstCommands.add(m_strParams);
                    }

                    pb.command(lstCommands);

                    System.out.println(lstCommands.toString());
                    Process tmp1 = pb.start();
                    while (tmp1.isAlive()) {
                        continue;
                    }
                    if (pb.redirectErrorStream()) {
                        System.err.println(MESSAGE_PROBLEM);
                        System.exit(0);
                    }

                    lstCommands.set(4, "\"power/energy-pkg/\"");

                    pb.command(lstCommands);

                    Process tmp2 = pb.start();
                    while (tmp2.isAlive()) {

                    }
                    if (pb.redirectErrorStream()) {
                        System.err.println(MESSAGE_PROBLEM);
                        System.exit(0);
                    }

                    pb.command(execNameTmp);

                    Process tmp3 = pb.start();

                    long nOldTime1 = System.currentTimeMillis();
                    while (tmp3.isAlive()) {
                        if ((System.currentTimeMillis() - nOldTime1) > TIME_LIMIT_TO_MEASURE_RUNTIME) {
                            lstExecutionModel.removeAll(lstExecutionModel);
                            return;
                        }
                    }
                    double sToltaTime1 = (System.currentTimeMillis() - nOldTime1) / 1000f;

                    RuntimeOutput tmp1Output = RuntimeService.gettingRuntimeOutput(tmp1); //perf do mem
                    RuntimeOutput tmp2Output = RuntimeService.gettingRuntimeOutput(tmp2); // perf do pkg

                    execModel.m_sMemenergy = RuntimeService.gettingJouleFromPerfsOutput(tmp1Output);
                    execModel.m_sCpuenergy = RuntimeService.gettingJouleFromPerfsOutput(tmp2Output);
                    execModel.m_sRuntime = sToltaTime1;
                }
            } catch (IOException ex) {
                RuntimeService.printStackTraceException(ex.getStackTrace());
                System.exit(0);
            }
        }
    }
}

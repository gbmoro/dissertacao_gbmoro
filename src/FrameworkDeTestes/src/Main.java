
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import step.step_2.Step2Model;
import step.step_2.ExperimentControllerStep2;
import step.step_2.ExperimentDesigner;
import service.FileManager;
import service.FileManagerType;
import service.RuntimeService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gbmoro
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Which step do you want?");
        System.out.printf("[1] Step 1\n[2] Step 2\n");
        int nStep = input.nextInt();

        switch (nStep) {
            case 1:
                if(RuntimeService.amISudo()) {
                    if(RuntimeService.isTherePAPITool()) {
                        if(RuntimeService.isThereScorepInstallation()) {
                            
                        } else {
                            RuntimeService.printErrorLog("You haven't the scorep tool...");
                        }
                    } else {
                        RuntimeService.printErrorLog("You haven't the papi-tool...");
                    }
                } else {
                    RuntimeService.printErrorLog("For this, you must be super user...");
                }
                break;
            case 2:
                if (RuntimeService.amISudo()) {
                    System.out.print("LIBNINA Configuration File Path:\n");
                    String strPath = input.next();
                    if (new File(strPath).exists()) {
                        exec_step2(strPath);
                    } else {
                        RuntimeService.printErrorLog("File doesn't exist...");
                        System.exit(0);
                    }
                } else {
                    RuntimeService.printErrorLog("For this, you must be super user...");
                }
                break;
            default:
        }
    }

    static void printTheTemplateXMLFile() {
        System.out.printf("<?xml version=\"1.0\"?>\n<root>\n");
        System.out.printf("\t<configuration>\n");
        System.out.printf("\t\t<execname_without_libnina>MiniFE.xml</execname_without_libnina>\n");
        System.out.printf("\t\t<execname_with_libnina>MiniFEComOpari.xml</execname_with_libnina>\n");
        System.out.printf("\t\t<config_file>miniFE.conf</config_file>\n");
        System.out.printf("\t\t<libnina_path>/home/gbmoro/libnina/src</libnina_path>\n");
        System.out.printf("\t\t<max_frequency>2300</max_frequency>\n");
        System.out.printf("\t\t<min_frequency>1200</min_frequency>\n");
        System.out.printf("\t\t<path_result>/home/gbmoro/result</path_result>\n");
        System.out.printf("\t\t<executions_number>30</executions_number>\n");
        System.out.printf("\t\t<total_threads>20</total_threads>\n");
        System.out.printf("\t\t<step_to_threadsgeneration>3</step_to_threadsgeneration>\n");
        System.out.printf("\t</configuration>\n");
        System.out.printf("</root>\n");
    }

    public static void exec_step2(String a_strConfigurationFile) {
        if (a_strConfigurationFile.isEmpty()) {
            RuntimeService.printLog("Please put as argument the framework input file...\nBy Example:\n\n");
            printTheTemplateXMLFile();
            System.exit(0);
        } else {
            String strXMLFilePath = a_strConfigurationFile;
            HashMap<String, String> mapKeyByValue
                    = FileManager.readerXMLFileInput(strXMLFilePath);
            if (mapKeyByValue == null) {
                System.err.printf(FileManager.PROBLEM_MESSAGE);
                System.exit(0);
            } else {
                RuntimeService.printLog("Checking if the perf tool is installed......");
                if (RuntimeService.isTherePerfInstallation()) {
                    RuntimeService.printLog("Perf->Ok");
                    RuntimeService.printLog("Checking if theuserspace governor exits...");
                    if (RuntimeService.isThereTheUserspaceGovernor()) {
                        RuntimeService.printLog("Userspace governor->Ok");
                        try {
                            ExperimentDesigner experimentDesigner = new ExperimentDesigner(
                                    Integer.parseInt(mapKeyByValue.get(FileManagerType.TOTAL_THREADS)),
                                    Integer.parseInt(mapKeyByValue.get(FileManagerType.EXECUTIONS_NUMBER)),
                                    Integer.parseInt(mapKeyByValue.get(FileManagerType.STEP_TO_THREADS)));
                            ArrayList<Step2Model> designOfExperimentLst
                                    = experimentDesigner.getExecutionModelList();

                            if (designOfExperimentLst == null || designOfExperimentLst.isEmpty()) {
                                RuntimeService.printErrorLog("There is a problem in design of experimentgeneration...\nTry again...");
                                System.exit(0);
                            }

                            String execName = "";
                            String execNameWithLibNina = "";
                            String params = "";

                            if (mapKeyByValue.get(FileManagerType.EXEC_NAME).split(" ").length > 1) {
                                execName = mapKeyByValue.get(FileManagerType.EXEC_NAME).split(" ")[0];
                                execNameWithLibNina = mapKeyByValue.get(FileManagerType.EXEC_NAME_WITH_LIBNINA).split(" ")[0];
                                params = mapKeyByValue.get(FileManagerType.EXEC_NAME).replaceAll(execName, "");
                            } else {
                                execName = mapKeyByValue.get(FileManagerType.EXEC_NAME);
                                execNameWithLibNina = mapKeyByValue.get(FileManagerType.EXEC_NAME_WITH_LIBNINA);
                            }

                            ExperimentControllerStep2 experimentController = new ExperimentControllerStep2(
                                    Long.parseLong(mapKeyByValue.get(FileManagerType.MAX_FREQUENCY)),
                                    Long.parseLong(mapKeyByValue.get(FileManagerType.MIN_FREQUENCY)),
                                    mapKeyByValue.get(FileManagerType.CONFIG_FILE),
                                    mapKeyByValue.get(FileManagerType.LIBNINA_PATH),
                                    execName,
                                    execNameWithLibNina,
                                    mapKeyByValue.get(FileManagerType.PATH_RESULT),
                                    params);

                            RuntimeService.hyperThreadingManager(false);
                            experimentController.runExperiment(designOfExperimentLst);
                            RuntimeService.hyperThreadingManager(true);

                            designOfExperimentLst.forEach(
                                    it -> System.out.println(it.toString())
                            );
                            if (FileManager.writeFile(mapKeyByValue.get(FileManagerType.PATH_RESULT), designOfExperimentLst)) {
                                RuntimeService.printLog("Experiment was a sucess");
                            } else {
                                RuntimeService.printErrorLog("Some problem occurred...");
                            }

                        } catch (Exception erro) {
                            RuntimeService.printStackTraceException(erro.getStackTrace());
                            System.exit(0);
                        }
                    } else {
                        RuntimeService.printLog("Userspace governor->NOk");
                        System.exit(0);
                    }
                } else {
                    RuntimeService.printErrorLog("Perf->NOk");
                    System.exit(0);
                }
            }
        }
    }
}

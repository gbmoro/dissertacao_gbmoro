/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 *
 * @author gbmoro
 */
public class RuntimeService {

    static final String CPUFREQ_BASE_PATH = "/sys/devices/system/cpu/";

    public static boolean amISudo() {
        try {
            Process p = Runtime.getRuntime().exec("id -u");
            RuntimeOutput routput = gettingRuntimeOutput(p);
            if (routput.m_strStd.equalsIgnoreCase("0")) {
                return true;
            }
        } catch (IOException ex) {
            printErrorLog(ex.getMessage());
        }
        return false;
    }

    public static RuntimeOutput gettingRuntimeOutput(Process a_pProcessExecuted) throws IOException {

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(a_pProcessExecuted.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(a_pProcessExecuted.getErrorStream()));

        String strStd = "";
        String strTmp = null;
        while ((strTmp = stdInput.readLine()) != null) {
            strStd += strTmp;
        }

        String strError = "";
        strTmp = null;
        while ((strTmp = stdError.readLine()) != null) {
            strError += strTmp;
        }

        return new RuntimeOutput(strStd, strError);
    }

    public static double gettingJouleFromPerfsOutput(RuntimeOutput a_roOutPut) {
        String withoutSpaces = a_roOutPut.m_strError.trim();
        String[] piecesOfWithoutSpaces = withoutSpaces.split(":");
        if (piecesOfWithoutSpaces.length == 2) {
            String[] piecesOfJoules = piecesOfWithoutSpaces[1].split("Joules");
            if (piecesOfJoules.length == 2) {
                return Double.parseDouble(piecesOfJoules[0].replace(",", "."));
            }
        }
        return 0f;
    }

    public static boolean isTherePerfInstallation() {
        try {
            String[] strCommandLines = new String[]{"/bin/sh",
                "-c",
                "ls /usr/bin/ | grep -w perf | wc -l"};

            Process p1 = Runtime.getRuntime().exec(strCommandLines);

            RuntimeOutput routput = gettingRuntimeOutput(p1);

            p1.destroy();

            return (Integer.parseInt(routput.m_strStd) >= 1);
        } catch (NumberFormatException | IOException ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            return false;
        }
    }

    public static void hyperThreadingManager(boolean isItToEnable) {

        RuntimeService.printLog("hyperThreadingManager call");

        String strToRegister = (isItToEnable) ? "1" : "0";

        File cpuFolders = new File(CPUFREQ_BASE_PATH);
        if (cpuFolders.exists()) {
            String[] allChildren = cpuFolders.list();
            int sizeAllChildrenLst = allChildren.length;
            for (int count = 0; count < sizeAllChildrenLst; count++) {
                if (allChildren[count].matches("cpu[0-9]*")) {
                    String strPathTarget = CPUFREQ_BASE_PATH + allChildren[count] + "/" + "online";
                    File cpuFileTmp = new File(strPathTarget);
                    if (cpuFileTmp.exists()) {
                        FileManager.writeHyperThreadingConfiguration(strPathTarget, strToRegister);
                        int nCpuNumber = Integer.parseInt(allChildren[count].replaceAll("\\D+", ""));
                        RuntimeService.printLog("Is the CPU " + nCpuNumber + " online? " + isItToEnable);
                    }
                }
            }
        }
    }

    public static boolean isThereTheUserspaceGovernor() {
        try {
            String[] strCommandLines = new String[]{"/bin/sh",
                "-c",
                "cpufreq-info -g | grep -c userspace"};

            Process p1 = Runtime.getRuntime().exec(strCommandLines);

            RuntimeOutput routput = gettingRuntimeOutput(p1);

            p1.destroy();

            return (Integer.parseInt(routput.m_strStd) >= 1);
        } catch (NumberFormatException | IOException ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            return false;
        }
    }

    public static boolean isTherePAPITool() {
        try {
            String[] strCommandLines = new String[]{"/bin/sh",
                "-c",
                "ls /usr/local/bin/ | grep -i papi_avail | wc -l"};

            Process p1 = Runtime.getRuntime().exec(strCommandLines);

            RuntimeOutput routput = gettingRuntimeOutput(p1);

            p1.destroy();

            return (Integer.parseInt(routput.m_strStd) >= 1);
        } catch (NumberFormatException | IOException ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            return false;
        }
    }
    
    public static boolean isThereScorepInstallation(){
        try {
            String[] strCommandLine1 = new String[]{"/bin/sh",
                "-c",
                "scorep --help | wc -l"};

            Process p1 = Runtime.getRuntime().exec(strCommandLine1);
            RuntimeOutput routput1 = gettingRuntimeOutput(p1);
            p1.destroy();

            return (Integer.parseInt(routput1.m_strStd) >= 20);
        } catch (NumberFormatException | IOException ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            return false;
        }
    }
    
    public static void printLog(String a_strMessage) {
        Date date = new Date();
        String strData = date.toString();
        System.out.print(strData + ":_Frame_teste: " + a_strMessage + "\n");
    }

    public static void printErrorLog(String a_strMessage) {
        Date date = new Date();
        String strData = date.toString();
        System.err.print(strData + ":_Frame_teste: " + a_strMessage + "\n");
    }

    public static void printStackTraceException(StackTraceElement[] ste) {
        if (ste == null || ste.length == 0) {
            return;
        } else {
            int count = 0;
            String msg;
            while (count < ste.length) {
                msg = ste[count].getFileName() + "->"
                        + ste[count].getLineNumber() + ":" + "_"
                        + ste[count].getMethodName();
                printErrorLog(msg);
                count++;
            }
        }
    }
}

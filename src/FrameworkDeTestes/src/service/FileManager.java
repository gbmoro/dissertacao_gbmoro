/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import step.step_2.Step2Model;

/**
 *
 * @author gbmoro
 */
public class FileManager {

    public static final String PROBLEM_MESSAGE = "Check your template, something wrong happended in the XML reading...\n";
    private static final String FILE_NAME_RESULT = "LIBNinaResultByFramework.csv";

    /**
     * Escreve o resultado da etapa2 em um path.
     * 
     * @param resultPath
     * @param a_lstExecutionModel
     * @return 
     */
    public static boolean writeFile(String resultPath, ArrayList<Step2Model> a_lstExecutionModel) {
        try {

            File folder = new File(resultPath);
            if (!folder.exists()) {
                folder.mkdir();
            }

            File file = new File(resultPath + File.separator + FILE_NAME_RESULT);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            PrintWriter writer = new PrintWriter(file);

            if (a_lstExecutionModel != null && !a_lstExecutionModel.isEmpty()) {

                String strHeader = "IsItWithLiBNina,threads,cpuenergy,memenergy,runtime";
                writer.println(strHeader);

                for (Step2Model step2ModelObject : a_lstExecutionModel) {
                    String tmpLine
                            = String.valueOf(step2ModelObject.m_bIsItWithLibNina) + ","
                            + String.valueOf(step2ModelObject.getThreadsNumber()) + ","
                            + String.valueOf(step2ModelObject.m_sCpuenergy) + ","
                            + String.valueOf(step2ModelObject.m_sMemenergy) + ","
                            + String.valueOf(step2ModelObject.m_sRuntime);
                    writer.println(tmpLine);
                }
                writer.close();
                return true;
            }
        } catch (Exception ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
        }
        return false;
    }

    /**
     * Escreva a configuração para desabilitar o hyperthreading via file object.
     *
     * @param strPath
     * @param strToReplace
     * @return
     */
    public static boolean writeHyperThreadingConfiguration(String strPath, String strToReplace) {

        try {
            File folder = new File(strPath);
            if (!folder.exists()) {
                folder.mkdir();
            }

            File file = new File(strPath);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            PrintWriter writer = new PrintWriter(file);
            writer.println(strToReplace);

            writer.close();
            return true;
        } catch (Exception ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            return false;
        }
    }

    /**
     * Esse método transforma um xml em um mapa de chave/valor.
     *
     * @param a_strPath
     * @return
     */
    public static HashMap<String, String> readerXMLFileInput(String a_strPath) {
        try {

            HashMap<String, String> hashMapOfValues = new HashMap<String, String>();

            File flFile = new File(a_strPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(flFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("configuration");

            Node nNodeTarget = nList.item(0);

            if (nNodeTarget.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNodeTarget;

                hashMapOfValues.put(FileManagerType.EXEC_NAME, eElement.getElementsByTagName(FileManagerType.EXEC_NAME).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.EXEC_NAME_WITH_LIBNINA, eElement.getElementsByTagName(FileManagerType.EXEC_NAME_WITH_LIBNINA).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.CONFIG_FILE, eElement.getElementsByTagName(FileManagerType.CONFIG_FILE).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.LIBNINA_PATH, eElement.getElementsByTagName(FileManagerType.LIBNINA_PATH).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.MAX_FREQUENCY, eElement.getElementsByTagName(FileManagerType.MAX_FREQUENCY).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.MIN_FREQUENCY, eElement.getElementsByTagName(FileManagerType.MIN_FREQUENCY).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.PATH_RESULT, eElement.getElementsByTagName(FileManagerType.PATH_RESULT).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.EXECUTIONS_NUMBER, eElement.getElementsByTagName(FileManagerType.EXECUTIONS_NUMBER).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.TOTAL_THREADS, eElement.getElementsByTagName(FileManagerType.TOTAL_THREADS).item(0).getTextContent());
                hashMapOfValues.put(FileManagerType.STEP_TO_THREADS, eElement.getElementsByTagName(FileManagerType.STEP_TO_THREADS).item(0).getTextContent());
                return hashMapOfValues;
            }

        } catch (Exception ex) {
            RuntimeService.printStackTraceException(ex.getStackTrace());
            RuntimeService.printErrorLog(PROBLEM_MESSAGE);
        }
        return null;
    }

}

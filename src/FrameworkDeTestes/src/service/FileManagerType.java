/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

/**
 * FileManagerType define tipos utilizados pela classe FileManager
 * 
 * @author gbmoro
 */
public interface FileManagerType {

    //BENCHMARK
    static final String EXEC_NAME = "execname_without_libnina";
    static final String EXEC_NAME_WITH_LIBNINA = "execname_with_libnina";
    //LIBNINA
    static final String CONFIG_FILE = "config_file";
    static final String LIBNINA_PATH = "libnina_path";
    static final String MAX_FREQUENCY = "max_frequency";
    static final String MIN_FREQUENCY = "min_frequency";
    //EXPERIMENT
    static final String PATH_RESULT = "path_result";
    static final String EXECUTIONS_NUMBER = "executions_number";
    static final String TOTAL_THREADS = "total_threads";
    static final String STEP_TO_THREADS = "step_to_threadsgeneration";
}
